package java.oop.inheritance;
// Extends the boat class so that it inherits some methods and variables. 
public class CargoShip extends Boat {
    double weightLimit; 
    int containerCapacity; 

    // Cargoship constructor defines different data than the boat class. 
    public CargoShip(double speed, double torque, double displacement, double weightLimit, int containerCapacity) {
        // gets this data from the boat class. 
        super(speed, torque, displacement);
        this.weightLimit = weightLimit;
        this.containerCapacity = containerCapacity;
    }
    //Override annotation tells the compiler that the printData() method is different inside the cargo ship class. 
    
    @Override
    public void printData() {
        System.out.println("Speed: " + speed);
        System.out.println("Torque: " + torque);
        System.out.println("Displacement:" + displacement);
        System.out.println("Weight Limit: " + weightLimit);
        System.out.println("containerCapacity: "+ containerCapacity);
    }
    
}
