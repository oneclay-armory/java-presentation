package java.oop.inheritance;
// Creates class called Boat. 
public class Boat {
    double speed;
    double torque; 
    double displacement; 
    // This is  a constructor and it is used to pass in parameters to a class. 
    public Boat(double speed, double torque, double displacement ) {
        this.speed = speed;
        this.torque = torque; 
        this.displacement = displacement; 
    }
// Use the parameters inside the constructor to print data. 
    public void printData() {
        System.out.println("Speed: " + speed);
        System.out.println("Torque: " + torque);
        System.out.println("Displacement:" + displacement);
    }
}
