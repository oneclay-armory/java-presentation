package java.oop.objects;



public class Car {
    //Defining values that are associated with  a car

    double Speed; 
    double Torque; 
    double Acceleration;

    /*
     * These parameters are values that we want to include with the Car object in future files. To do this we make a constructor that stores the values for the car object.
     * These values are then told that they are equal to the values that we previously listed at the top of the file. Then, these values are printed in the methods\
     * that are below this constructor. 
     */
    public Car(double Speed, double Torque, double Acceleration) {
        // The this keyword tells the computer that the values in the class should equal the values in the constructor.  
        this.Speed = Speed; 
        this.Torque = Torque;
        this.Acceleration = Acceleration;
    }
    // Each of these methods print the data that is defined in the constructor 
    public void printSpeed( ){
        System.out.println("Speed: " + Speed + "m/s");
    }
    
    public void printTorque( ) {
        System.out.println("Torque: " + Torque + "N*m");
    }

    public void printAccel() {
        System.out.println("Acceleration: " + Acceleration + "m/s^2");
    }
    // Simple example of using a method to perform multiple tasks quickly. 
    public void printAllData() {
        printSpeed();
        printTorque();
        printAccel();
    }

}
