package java.conditionals;

public class whileLoops {
    
    public static void main(String[] args) {    
        
        int i = 0;
        // Tells the code that it wants to perform this task while integer i is less than or equal to 100
        do {
            System.out.println("Integer i has not met its target value");
            i++;
        }
        
        while(i <= 100);

    } 
    
}
