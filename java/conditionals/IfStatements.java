package java.conditionals;

import java.util.Scanner;

public class IfStatements {
    // This code is kinda complex for no reason but I will explain every part of it. 
    public static void main(String[ ] args) {
        // Explanation message
        System.out.println("This is an example of how to use if statements. The correct values of integers i and p aer 10 and 6 respectively. If they are correct the program will run the code in the else block");
        int i;
        int p;
        // Define new scanner object 
        Scanner intVals = new Scanner(System.in);
        // Asks user to input values for intergers i & p. 
        System.out.println("What value is integer i");
        i = intVals.nextInt();
        System.out.println("What value is integer p");
        p = intVals.nextInt();
        // Tells the user what their input was. 
        System.out.println("You said i was " + i);
        System.out.println("You said was " + p);
        // This if statement tests to see if the integers i and p are not equal to 10 and 6. If they are nto equal to 10 and 6 it prints out the information inside of the code block
        // If it is correct it prints out the data inside of the 
        if(i != 10 && p != 6) {
            System.out.println("The integer is the incorrect value.");
            System.out.println("The incorrect value for i is: " + i);
            System.out.println("The  incorrectvalue for p is: " + p);
        }
        // Tests to see if i is equal to 10 and p is not equal to 6. If it is true  it pritns out the data inside of this if statement. 
        else if ( i == 10 && p != 6) {
            System.out.println("Integer i is correct");
            System.out.println("Integer P is incorrect");

        }
        // Does the same thing as the previous block but tests the opposite number. 
        else if (i != 10 && p == 6) {
            System.out.println("Integer i is incorrect ");
            System.out.println("Integer p is correct");
        }
        // This tells the user that the integers they inputted are correct. 
        else {
            System.out.println("The integers are the correct value.");
            System.out.println("Integer i equals " + i);
            System.out.println("Integer p equals " + p);
        }
        // closes the scanner 
        intVals.close();

    }
    
}
